

public class Book {
    private String title;
    private String date;
    private String author;
    private static  int id=1;
    private int idBook;
    private boolean rented=false;
    private String rentedPersonName="";


    public Book(String title, String date, String author) {
        this.title = title;
        this.date = date;
        this.author = author;
        idBook=id;
        id++;
    }


    public String getRentedPersonName() {
        return rentedPersonName;
    }



    public void setRentedPersonName(String rentedPersonName) {
        this.rentedPersonName = rentedPersonName;
    }

    public boolean isRented() {
        return rented;
    }

    public String getTitle() {

        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }


    public int getIdBook() {
        return idBook;
    }

    public void setIdBook(int idBook) {
        this.idBook = idBook;
    }

    public void setRented(boolean rented){
        this.rented=rented;
    }

    @Override
    public String toString() {
        if(getRentedPersonName().equals("")){
            return "Book{" +
                    "idBook=" + idBook +
                    ", title='" + title + '\'' +
                    ", date='" + date + '\'' +
                    ", author='" + author + '\'' +
                    ", rented=" + rented +
                    '}';
        }
        else{
            return "Book{" +
                    "idBook=" + idBook +
                    ", title='" + title + '\'' +
                    ", date='" + date + '\'' +
                    ", author='" + author + '\'' +
                    ", rented=" + rented +
                    ", rentedPersonName='" + rentedPersonName + '\'' +
                    '}';
        }
    }


}
