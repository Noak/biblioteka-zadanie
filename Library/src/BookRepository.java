
import java.util.ArrayList;
import java.util.Scanner;

public class BookRepository {

    private static ArrayList<Book> books=new ArrayList<>();
    private static int rentedNow;
    private static int choiceInt;
    final static boolean condition=true;
    private static String  choice;
    private static String titleBook;
    private static String dateBook;
    private static String authorBook;
    private static String titleS="";
    private static String dateS="";
    private static String authorS="";
    private static String  idString;
    private static int id;
    private static boolean isExist=false;
    private static int iter;
    private static int cout;


    public static void main(String[] args) {

        Scanner read = new Scanner(System.in);
        System.out.println("Welcome in Library management systems :) --->>>> ");
        System.out.println();


        while(condition){
            System.out.println("If you want to add a book to the library,  press 1");
            System.out.println("If you want to search for a book press, 2");
            System.out.println("If you want to see a list of books, press 3");
            System.out.println("If you wan borrow a book, press 4");
            System.out.println("Remove a book from the library, press 5");
            System.out.println("Return book to library, press 6");
            System.out.println("Exit, press 0");

            choice = read.nextLine();
            while(isString(choice)!=true){
                choice = read.nextLine();
                System.out.println("Not a number , Try Again");


            }
            choiceInt = Integer.parseInt(choice);

            System.out.println();
            switch(choiceInt){

                case 0:
                    System.exit(0);
                case 1:

                        System.out.println("Give the title of the book: ");
                        titleBook = read.nextLine();
                        while(isEmptyM(titleBook)==true){
                            System.out.println("Give the title of the book: ");
                            titleBook = read.nextLine();
                        }
                        System.out.println("Enter the year of publication of the book: ");
                        dateBook = read.nextLine();
                        while(isNumber(dateBook)!=true){
                            System.out.println("Enter the year of publication of the book: ");
                            dateBook = read.nextLine();
                        }

                        System.out.println("Enter the author of: ");
                        authorBook = read.nextLine();
                        while(isEmptyM(authorBook)==true){
                            System.out.println("Enter the author of: ");
                            authorBook = read.nextLine();
                        }
                        books.add(new Book(titleBook,dateBook,authorBook));
                        System.out.println();
                        System.out.println("-------------------------------------------------------");

                    break;

                case 2:


                    System.out.println("Want to search by title? Write: 'yes'");
                    String answer = read.nextLine();
                    cout=0;
                    if (answer.equals("yes")) {
                        System.out.println("Tile: ");
                         titleS = read.nextLine();
                        while(isEmptyM(titleS)==true){
                            System.out.println("Tile: ");
                            titleS = read.nextLine();
                        }
                         cout=cout+2;
                    }

                    System.out.println("Want to search by year? Write: 'yes'");
                    answer = read.nextLine();
                    if (answer.equals("yes")) {
                        System.out.println("Yer: ");
                        dateS = read.nextLine();
                        while(isNumber(dateS)!=true){
                            System.out.println("Yer: ");
                            dateS = read.nextLine();
                        }
                        cout=cout+5;
                    }

                    System.out.println("Want to search by author? Write: 'yes'");
                    answer = read.nextLine();
                    if (answer.equals("yes")) {
                        System.out.println("Autor: ");
                        authorS = read.nextLine();
                        while(isEmptyM(authorS)==true){
                            System.out.println("Autor: ");
                            authorS = read.nextLine();
                        }
                        cout=cout+3;
                    }


                    if(cout==10){
                        for (Book book : books) {
                            if(book.getTitle().equals(titleS) && book.getDate().equals(dateS) && book.getAuthor().equals(authorS)){
                                System.out.println(book.toString());
                            }
                        }

                    }
                    if(cout==7){
                        for (Book book : books) {
                            if(book.getTitle().equals(titleS) && book.getDate().equals(dateS)){
                                System.out.println(book.toString());
                            }
                        }
                    }
                    if (cout==8){
                        for (Book book : books) {
                            if(book.getTitle().equals(titleS) && book.getAuthor().equals(authorS)){
                                System.out.println(book.toString());
                            }
                        }
                    }

                     if (cout==5){
                        for (Book book : books) {
                            if(book.getDate().equals(dateS)){
                                System.out.println(book.toString());
                            }
                        }
                    }

                    if (cout==3){
                        for (Book book : books) {
                            if(book.getAuthor().equals(authorS)){
                                System.out.println(book.toString());
                            }
                        }
                    }

                    if (cout==2){
                        for (Book book : books) {
                            if(book.getTitle().equals(titleS)){
                                System.out.println(book.toString());
                            }
                        }
                    }
                    System.out.println();
                    System.out.println("-------------------------------------------------------");

                    break;

                case 3:

                    for (Book book : books) {
                        System.out.println(book.toString());
                        if(book.isRented()==true){
                            ++rentedNow;
                        }
                    }
                    System.out.print("All books: ");
                    System.out.println(books.size());
                    System.out.print("Currently rented: ");
                    System.out.println(rentedNow);
                    System.out.println();
                    System.out.println("-------------------------------------------------------");


                    break;

                case 4:

                    System.out.println("Enter the id book ");
                    idString = read.nextLine();
                    while(isNumber(idString)!=true){
                        System.out.println("Enter the id book ");
                        idString = read.nextLine();
                    }
                    id=Integer.parseInt(idString);
                    for (Book book : books) {
                        if (book.getIdBook()==id){
                            if(book.isRented()==true){
                                System.out.println("The book has already been rented");
                                System.out.println("The book has: "+book.getRentedPersonName());
                            }
                            else {
                                book.setRented(true);
                                System.out.println("Name and surname of borrower");
                                book.setRentedPersonName(read.nextLine());
                            }
                        }
                    }
                    System.out.println();
                    System.out.println("-------------------------------------------------------");


                    break;

                case 5:

                    System.out.println("Enter the id book to delete: ");
                    idString = read.nextLine();
                    while(isNumber(idString)!=true){
                        System.out.println("Enter the id book to delete: ");
                        idString = read.nextLine();
                    }

                    id=Integer.parseInt(idString);

                    for (Book book : books) {
                        ++iter;
                        if (book.getIdBook()==id){
                            isExist=true;
                            if(book.isRented()==true){
                                System.out.println("The book has already been rented, Can not delete!");
                                System.out.println("The book has: "+book.getRentedPersonName());
                            }
                            else {
                                books.remove(iter-1);
                                System.out.println("The book has been deleted!");
                            }
                        }
                    }
                    if(isExist!=true){
                        System.out.println("The book does not exist");
                    }
                    System.out.println();
                    System.out.println("-------------------------------------------------------");


                    break;

                case 6:

                    System.out.println("Enter the id book to return: ");
                    idString = read.nextLine();
                    while(isNumber(idString)!=true){
                        System.out.println("Enter the id book to delete: ");
                        idString = read.nextLine();
                    }

                    id=Integer.parseInt(idString);

                    for (Book book : books) {
                        if (book.getIdBook()==id){
                            isExist=true;
                            if(book.isRented()==true){
                                book.setRented(false);
                                book.setRentedPersonName("");
                                System.out.println("OK, The book returned");
                            }
                            else {
                                System.out.println("The book is not rented");
                            }
                        }
                    }
                    if(isExist!=true){
                        System.out.println("The book does not exist");
                    }
                    System.out.println();
                    System.out.println("-------------------------------------------------------");


                    break;





            }
        }
    }
    public static boolean isNumber(String string) {
        try {
            Long.parseLong(string);
        } catch (Exception e) {
            return false;
        }
        return true;
    }
    public static boolean isString(String string) {

        try {
            Integer.parseInt(string);

        } catch (NumberFormatException ex) {
            return false;
        }
        return true;
    }

    public static boolean isEmptyM(String string) {
        if(string.equals("")){
            return true;

        }
        else{
            return false;
        }
    }






}
